import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder,FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";



@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent implements OnInit {
    registerForm: FormGroup;
    public Id;
    public navigateTo: string = '/users/index';
    public imageSrc: string = '';
    public Items: any[] = [];
    public changeImage;
    public rowsNames: any[] = ['שם מלא','סיסמה','טלפון','אימייל','כתובת'];
    public rows: any[] = ['name','password','phone','email','address'];
    public Item;
    public schoolgradeArray : any = [];
    public teachinglevelArray : any = [];
    public branchesArray : any = [];


    public host;
    public Image;
    public SubCategories;
    public isReady;
    public Change: boolean = false;
    @ViewChild("fileInput") fileInput;

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService) {

        this.route.params.subscribe(params => {
            this.Id = params['id'];
            this.Item = this.service.Items[this.Id];
            this.host = settings.host;
            //this.Item.Change  = false;
            //this.isReady = true;
            console.log("Product", this.Item)
            console.log("Item : " , this.Item)
        });
    }


    onSubmit(form: NgForm) {


        // let fi = this.fileInput.nativeElement;
         let fileToUpload;
        // if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        // this.Item.Change = this.Change;
        //
         console.log("EditUser",form.value);

        this.service.EditItem('WebEditUser',form.value,fileToUpload).then((data: any) => {
            console.log("WebEditUser : " , data);
            this.router.navigate([this.navigateTo]);
        });

    }

    ngOnInit() {

        this.registerForm = new FormGroup({
            'id':new FormControl(this.Item.id),
            //'username':new FormControl(this.Item.username,Validators.required),
            'password':new FormControl(this.Item.password,Validators.required),
            'name':new FormControl(this.Item.name,Validators.required),
            'phone':new FormControl(this.Item.phone,[Validators.required, Validators.minLength(9),Validators.pattern('^[0-9]+$')]),
            'email':new FormControl(this.Item.email,[Validators.required, Validators.email]),
            'address':new FormControl(this.Item.address),
        })

    }


    onChange(event) {

        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
