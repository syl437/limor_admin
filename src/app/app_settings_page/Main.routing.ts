import {Routes} from '@angular/router';


import {EditComponent} from "./index/edit.component";

export const MaindRoutes: Routes = [{
    path: '',
    children: [{
        path: 'index',
        component: EditComponent,
        data: {
            heading: 'הגדרות אפליקציה'
        }
    }]
}];
