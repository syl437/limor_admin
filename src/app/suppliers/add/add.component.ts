import {Component, OnInit, ElementRef, ChangeDetectionStrategy, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";

import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormControl, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";

import {FileUploader, FileUploaderOptions} from 'ng2-file-upload/ng2-file-upload';


@Component({
    selector: 'app-add',
    templateUrl: './add.component.html',
    styleUrls: ['./add.component.css']
})
export class AddComponent {

    public navigateTo: string = '/suppliers/index';
    public paramsSub;
    public id: number;
    public Categories:any[]=[];
    public imageSrc: string = '';
    public isReady:Boolean = true;
    @ViewChild("fileInput") fileInput;
    public subId = ''

    public Product =
        {
            'title':'',
            //'image':''
        }

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router) {

    }

    onSubmit(form: NgForm) {

        //let fi = this.fileInput.nativeElement;
        let fileToUpload;
        //if (fi.files && fi.files[0]) {fileToUpload = fi.files[0];}
        //console.log("Vl : " , form.value  , fileToUpload);

         if(form.value.title == "")
             alert ("הכנס שם הספק")

        else
         {
             this.service.AddItem('WebAddSupplier',form.value,fileToUpload).then((data: any) => {
                 console.log("WebAddSupplier : " , data);
                 this.router.navigate([this.navigateTo]);
             });
         }
    }




}
