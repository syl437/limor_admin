import {Component, OnInit, ElementRef, ViewChild, NgZone} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FileUploader} from "ng2-file-upload";
import {Http, RequestOptions} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {Headers} from '@angular/http';
import {FormBuilder, FormGroup, NgForm, Validators} from "@angular/forms";
import {Ng2UploaderModule} from 'ng2-uploader';
import {MainService} from "../MainService.service";
import {SettingsService} from "../../../settings/settings.service";
import {Location} from '@angular/common';


const URL = 'https://evening-anchorage-3159.herokuapp.com/api/';

@Component({
    selector: 'app-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../components/buttons/buttons.component.scss', './edit.component.css']
})

export class EditComponent {

    public Id;
    public navigateTo: string = '/products/index';
    public imageSrc: string = '';
    public Items: any[] = [];
    public changeImage;
    public rowsNames: any[] = ['שם','מחיר נמוך','מחיר גבוה'];
    public rows: any[] = ['title','low_price','high_price'];
    public Item;
    public host;
    public Image;
    public SubCategories;
    public Categories;
    public isReady;
    public Change: boolean = false;
    public suppliersArray:any = [];
    @ViewChild("fileInput") fileInput;
    public IndexItem = 0;

    constructor(private route: ActivatedRoute, private http: Http, public service: MainService, public router: Router, public settings: SettingsService,private _location: Location) {

        this.route.params.subscribe(params => {
            this.Id = params['id'];


            for (let i = 0; i < this.service.Items.length; i++) {
                if (this.service.Items[i].id == this.Id) {
                    this.IndexItem = i;
                }
            }


            this.Item = this.Item = this.service.Items[this.IndexItem];//this.service.Items[this.Id];
            this.host = settings.host;

            this.service.GetItems('WebGetSuppliers',-1).then((data: any) => {
                this.suppliersArray = data;
                console.log("Sub : " , data);
            });



            let CategoryId:any = '';

            if (this.Item.category_id == 0) {
                this.Item.sub_category_id = "0";
                CategoryId = 0;
            } else {
                CategoryId = this.Item.category_id;
            }




            this.service.GetItems('GetSubCategoriesById',CategoryId).then((data: any) => {
                this.SubCategories = data;
                this.isReady = true;
                console.log("Sub : " , data);
            });
            console.log("Product", this.Item)
            console.log("Item : " , this.Item)
        });



        this.service.GetItems('GetCategories',-1).then((data: any) => {
            this.Categories = data;
            this.isReady = true;
        });


    }

    getSubCategories(evt) {
        this.service.GetItems('GetSubCategoriesById',evt.target.value).then((data: any) => {
            this.SubCategories = data;
            this.isReady = true;
        });
    }

    onSubmit(form: NgForm) {
        console.log("Edit1 : " , this.Item);
        let fi = this.fileInput.nativeElement;
        let fileToUpload;
        if (fi.files && fi.files[0]) {fileToUpload = fi.files[0]; console.log("fff : ",fileToUpload);}
        this.Item.change = this.Change.toString();

        //this.Item.change = this.Change;
        this.service.EditItem('EditProduct', this.Item, fileToUpload).then((data: any) => {
            console.log("AddCompany : ", data);
            this._location.back();
            //this.router.navigate([this.navigateTo]);
        });
    }


    onChange(event) {
        this.Change = true;
        var files = event.srcElement.files;
        this.changeImage = event.target.files[0];

        let reader = new FileReader();
        reader.onload = (e: any) => {
            this.changeImage = e.target.result;
        }
        reader.readAsDataURL(event.target.files[0]);
    }
}
