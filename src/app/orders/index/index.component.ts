import {Component, OnInit} from '@angular/core';
import {OrderService} from "../order.service";
import {SettingsService} from "../../../settings/settings.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
    selector: 'app-index',
    templateUrl: './index.component.html',
    styleUrls: ['../../icons/fontawesome/fontawesome.component.scss', '../../media/list/list.component.scss', './index.component.css']
})


export class IndexComponent implements OnInit {

    ItemsArray: any[] = [];
    ItemsArray1: any[] = [];
    host: string = '';
    Id:number;
   
    
    
    constructor(public OrderService:OrderService,public settings:SettingsService , private route: ActivatedRoute) {
          this.route.params.subscribe(params => {
              this.Id = params['id'];
              console.log("ssss : " , this.Id )
              this.OrderService.GetOrders('GetOrders', this.Id).then((data: any) => {
                  console.log("Orders : ", data) ,
                      this.ItemsArray = data,
                      this.ItemsArray1 = data,
                      this.host = settings.host
                      //console.log(this.ItemsArray[0].logo)
              })
        });

        this.OrderService.GetAllKitchens('GetAllKitchens').then((data: any) => {
            console.log("GetAllKitchens : ", data)
        })
    }

    ngOnInit() {
    }

    DeleteItem(i) {
        console.log("Del 1 : ", this.ItemsArray[i].id);
       /* this.OrderService.DeleteCompany('DeleteCompany', this.ItemsArray[i].id).then((data: any) => {
            this.ItemsArray = data , console.log("Del 2 : ", data);
        })*/
    }

    updateFilter(event) {
       /* const val = event.target.value;
        // filter our data
        const temp = this.ItemsArray1.filter(function (d) {
            return d.CompanyName[0]['name'].toLowerCase().indexOf(val) !== -1 || !val;
        });
        // update the rows
        this.ItemsArray = temp;*/
    }

}
